-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.22-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for uas_rpl
CREATE DATABASE IF NOT EXISTS `uas_rpl` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `uas_rpl`;

-- Dumping structure for table uas_rpl.buku
CREATE TABLE IF NOT EXISTS `buku` (
  `ID_buku` int(11) NOT NULL AUTO_INCREMENT,
  `Judul` varchar(50) DEFAULT NULL,
  `Pengarang` varchar(50) DEFAULT NULL,
  `Penerbit` varchar(50) DEFAULT NULL,
  `ID_jenis` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID_buku`) USING BTREE,
  KEY `FK_buku_jenis_buku` (`ID_jenis`),
  CONSTRAINT `FK_buku_jenis_buku` FOREIGN KEY (`ID_jenis`) REFERENCES `jenis_buku` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table uas_rpl.buku: ~2 rows (approximately)
/*!40000 ALTER TABLE `buku` DISABLE KEYS */;
REPLACE INTO `buku` (`ID_buku`, `Judul`, `Pengarang`, `Penerbit`, `ID_jenis`) VALUES
	(1, 'belajar ngoding', 'mona', 'elex media', 1),
	(2, 'ayo ngoding', 'agus', 'gramedia', 1);
/*!40000 ALTER TABLE `buku` ENABLE KEYS */;

-- Dumping structure for table uas_rpl.detail_pinjam
CREATE TABLE IF NOT EXISTS `detail_pinjam` (
  `ID_detail` int(11) NOT NULL AUTO_INCREMENT,
  `Tgl_pinjam` varchar(50) NOT NULL DEFAULT '0',
  `Tgl_kembali` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID_detail`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table uas_rpl.detail_pinjam: ~2 rows (approximately)
/*!40000 ALTER TABLE `detail_pinjam` DISABLE KEYS */;
REPLACE INTO `detail_pinjam` (`ID_detail`, `Tgl_pinjam`, `Tgl_kembali`) VALUES
	(1, '29-05-2021', '05-06-2021');
/*!40000 ALTER TABLE `detail_pinjam` ENABLE KEYS */;

-- Dumping structure for table uas_rpl.jenis_buku
CREATE TABLE IF NOT EXISTS `jenis_buku` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Jenis` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE,
  UNIQUE KEY `Jenis` (`Jenis`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table uas_rpl.jenis_buku: ~2 rows (approximately)
/*!40000 ALTER TABLE `jenis_buku` DISABLE KEYS */;
REPLACE INTO `jenis_buku` (`ID`, `Jenis`) VALUES
	(2, 'Ekonomi'),
	(1, 'Pemrograman');
/*!40000 ALTER TABLE `jenis_buku` ENABLE KEYS */;

-- Dumping structure for table uas_rpl.peminjam
CREATE TABLE IF NOT EXISTS `peminjam` (
  `ID_peminjam` int(11) NOT NULL AUTO_INCREMENT,
  `Nama_peminjam` varchar(50) DEFAULT NULL,
  `Gender` varchar(50) DEFAULT NULL,
  `Tgl_lahir` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID_peminjam`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table uas_rpl.peminjam: ~2 rows (approximately)
/*!40000 ALTER TABLE `peminjam` DISABLE KEYS */;
REPLACE INTO `peminjam` (`ID_peminjam`, `Nama_peminjam`, `Gender`, `Tgl_lahir`) VALUES
	(2, 'Sandi', 'Pria', '27-03-2002'),
	(3, 'Usamah', 'Pria', '30-01-2001');
/*!40000 ALTER TABLE `peminjam` ENABLE KEYS */;

-- Dumping structure for table uas_rpl.pinjam
CREATE TABLE IF NOT EXISTS `pinjam` (
  `ID_pinjam` int(11) NOT NULL AUTO_INCREMENT,
  `ID_peminjam` int(11) DEFAULT NULL,
  `ID_buku` int(11) DEFAULT NULL,
  `ID_detail` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID_pinjam`),
  KEY `FK_detail_pinjam_pinjam` (`ID_detail`) USING BTREE,
  KEY `FK_pinjam_peminjam` (`ID_peminjam`),
  KEY `FK_pinjam_buku` (`ID_buku`),
  CONSTRAINT `FK_pinjam_buku` FOREIGN KEY (`ID_buku`) REFERENCES `buku` (`ID_buku`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_pinjam_detail_pinjam` FOREIGN KEY (`ID_detail`) REFERENCES `detail_pinjam` (`ID_detail`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_pinjam_peminjam` FOREIGN KEY (`ID_peminjam`) REFERENCES `peminjam` (`ID_peminjam`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table uas_rpl.pinjam: ~1 rows (approximately)
/*!40000 ALTER TABLE `pinjam` DISABLE KEYS */;
REPLACE INTO `pinjam` (`ID_pinjam`, `ID_peminjam`, `ID_buku`, `ID_detail`) VALUES
	(1, 3, 1, 1);
/*!40000 ALTER TABLE `pinjam` ENABLE KEYS */;

-- Dumping structure for table uas_rpl.pustakawan
CREATE TABLE IF NOT EXISTS `pustakawan` (
  `ID_pustakawan` int(11) NOT NULL AUTO_INCREMENT,
  `Nama_pustakawan` varchar(50) DEFAULT NULL,
  `Gender` varchar(50) DEFAULT NULL,
  `Tgl_lahir` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID_pustakawan`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table uas_rpl.pustakawan: ~0 rows (approximately)
/*!40000 ALTER TABLE `pustakawan` DISABLE KEYS */;
/*!40000 ALTER TABLE `pustakawan` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
